/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
	  |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include "Planet.h"
#include <OgreManualObject.h>


//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{

}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
	sun = new Planet(cubeNode);
	sun->createPlanet(mSceneMgr, 10, ColourValue::ColourValue(1, 1, 0), "BaseWhiteNoLighting");
	sun->setRevolutionSpeed(0);
	sun->setLocalRotationSpeed(1);

	MaterialPtr mercuryMaterial = Ogre::MaterialManager::getSingleton().create("mercury", "General");
	mercuryMaterial->setReceiveShadows(true);
	mercuryMaterial->getTechnique(0)->setLightingEnabled(true);
	mercuryMaterial->getTechnique(0)->getPass(0)->setDiffuse(2.3, 2.3, 2.3, 0);
	mercuryMaterial->getTechnique(0)->getPass(0)->setAmbient(ColourValue(0.82f, 0.7f, 0.54));

	mercury = new Planet(cubeNode);
	mercury->createPlanet(mSceneMgr, 3, ColourValue::ColourValue(0.82f, 0.7f, 0.54), "mercury");
	mercury->setParent(sun);
	mercury->getNode()->setPosition(mercury->getParent()->getNode()->getPosition().x + 100, 0, 0);
	mercury->setRevolutionSpeed(0.24); // 88 / 365
	mercury->setLocalRotationSpeed(1);

	MaterialPtr venusMaterial = Ogre::MaterialManager::getSingleton().create("venus", "General");
	venusMaterial->setReceiveShadows(true);
	venusMaterial->getTechnique(0)->setLightingEnabled(true);
	venusMaterial->getTechnique(0)->getPass(0)->setDiffuse(4, 4, 4, 0);
	venusMaterial->getTechnique(0)->getPass(0)->setAmbient(ColourValue(0.93, 0.9f, 0.67f));

	venus = new Planet(cubeNode);
	venus->createPlanet(mSceneMgr, 5, ColourValue::ColourValue(0.93, 0.9f, 0.67f), "venus");
	venus->setParent(sun);
	venus->getNode()->setPosition(venus->getParent()->getNode()->getPosition().x + 150, 0, 0);
	venus->setRevolutionSpeed(0.61); // 224 / 365
	venus->setLocalRotationSpeed(1);

	MaterialPtr earthMaterial = Ogre::MaterialManager::getSingleton().create("earth", "General");
	earthMaterial->setReceiveShadows(true);
	earthMaterial->getTechnique(0)->setLightingEnabled(true);
	earthMaterial->getTechnique(0)->getPass(0)->setDiffuse(5, 5, 5, 0);
	earthMaterial->getTechnique(0)->getPass(0)->setAmbient(ColourValue::Blue);


	earth = new Planet(cubeNode);
	earth->createPlanet(mSceneMgr, 10, ColourValue::ColourValue::Blue, "earth");
	earth->setParent(sun);
	earth->getNode()->setPosition(earth->getParent()->getNode()->getPosition().x + 200, 0, 0);
	earth->setRevolutionSpeed(1);
	earth->setLocalRotationSpeed(1);

	MaterialPtr moonMaterial = Ogre::MaterialManager::getSingleton().create("moon", "General");
	moonMaterial->setReceiveShadows(true);
	moonMaterial->getTechnique(0)->setLightingEnabled(true);
	moonMaterial->getTechnique(0)->getPass(0)->setDiffuse(7, 7, 7, 0);
	moonMaterial->getTechnique(0)->getPass(0)->setAmbient(ColourValue(0.7f, 0.7f, 0.7f));

	moon = new Planet(cubeNode);
	moon->createPlanet(mSceneMgr, 1, ColourValue::ColourValue(0.7f, 0.7f, 0.7f), "moon");
	moon->setParent(earth);
	moon->getNode()->setPosition(moon->getParent()->getNode()->getPosition().x + 180, 0, 0);
	moon->setRevolutionSpeed(5);
	moon->setLocalRotationSpeed(1);

	MaterialPtr marsMaterial = Ogre::MaterialManager::getSingleton().create("mars", "General");
	marsMaterial->setReceiveShadows(true);
	marsMaterial->getTechnique(0)->setLightingEnabled(true);
	marsMaterial->getTechnique(0)->getPass(0)->setDiffuse(9, 9, 9, 0);
	marsMaterial->getTechnique(0)->getPass(0)->setAmbient(ColourValue(0.71f, 0.25f, 0.05f));

	mars = new Planet(cubeNode);
	mars->createPlanet(mSceneMgr, 8, ColourValue::ColourValue(0.71f, 0.25f, 0.05f), "mars");
	mars->setParent(sun);
	mars->getNode()->setPosition(mars->getParent()->getNode()->getPosition().x + 250, 0, 0);
	mars->setRevolutionSpeed(1.88); // 687 / 365
	mars->setLocalRotationSpeed(1);

	Light* pointLight = mSceneMgr->createLight();
	pointLight->setType(Light::LightTypes::LT_POINT);
	pointLight->setPosition(Vector3(0, 0, 0));
	pointLight->setDiffuseColour(ColourValue(1.0f, 1.0f, 0.0f));
	pointLight->setSpecularColour(ColourValue(1.0f, 1.0f, 1.0f));
	pointLight->setAttenuation(325, 0.0f, 0.014, 0.0007);
	pointLight->setCastShadows(true);

	mSceneMgr->setAmbientLight(ColourValue(0.3f, 0.3f, 0.3f));
}

bool TutorialApplication::frameStarted(const FrameEvent& evt)
{
	sun->update(evt);
	mercury->update(evt);
	venus->update(evt);
	earth->update(evt);
	moon->update(evt);
	mars->update(evt);

	return true;
}

//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
	INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
	int main(int argc, char* argv[])
#endif
	{
		// Create application object
		TutorialApplication app;

		try {
			app.go();
		}
		catch (Ogre::Exception& e) {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
			MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
			std::cerr << "An exception has occurred: " <<
				e.getFullDescription().c_str() << std::endl;
#endif
		}

		return 0;
	}

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
