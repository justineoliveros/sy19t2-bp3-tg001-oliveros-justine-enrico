/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
#include "OgreManualObject.h"

//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
	velocity = 0;
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

ManualObject * TutorialApplication :: createCube(float size)
{
	ManualObject *triangle = mSceneMgr->createManualObject();
	triangle->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_LIST);

	// Front
	triangle->position(size, size, size); // UR
	triangle->colour(ColourValue::Blue);
	triangle->position(size, -size, size); // LR
	triangle->colour(ColourValue::Red);
	triangle->position(-size, -size, size); // LL
	triangle->colour(ColourValue::Green);
	triangle->position(-size, size, size); // UL
	triangle->colour(ColourValue::White);

	// Back
	triangle->position(size, size, -size); // UR
	triangle->colour(ColourValue::Blue);
	triangle->position(size, -size, -size); // LR
	triangle->colour(ColourValue::Red);
	triangle->position(-size, -size, -size); // LL
	triangle->colour(ColourValue::Green);
	triangle->position(-size, size, -size); // UL
	triangle->colour(ColourValue::White);

	// Front Index
	triangle->index(0);
	triangle->index(2);
	triangle->index(1);

	triangle->index(2);
	triangle->index(0);
	triangle->index(3);

	// Back Index
	triangle->index(4);
	triangle->index(5);
	triangle->index(6);

	triangle->index(6);
	triangle->index(7);
	triangle->index(4);

	// Right Index
	triangle->index(1);
	triangle->index(5);
	triangle->index(4);

	triangle->index(4);
	triangle->index(0);
	triangle->index(1);

	// Left Index
	triangle->index(3);
	triangle->index(6);
	triangle->index(2);

	triangle->index(7);
	triangle->index(6);
	triangle->index(3);

	// Top Index
	triangle->index(0);
	triangle->index(4);
	triangle->index(3);

	triangle->index(7);
	triangle->index(3);
	triangle->index(4);

	// Bottom Index
	triangle->index(1);
	triangle->index(2);
	triangle->index(5);

	triangle->index(6);
	triangle->index(5);
	triangle->index(2);

	// End drawing
	triangle->end();

	return triangle;
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	ManualObject* manual = createCube(10.0f);

	cubeNode = mSceneMgr->getRootSceneNode()->createChildSceneNode();
	cubeNode->attachObject(manual);

	cubeNode->translate(20, 0, 0);
}


bool TutorialApplication::frameStarted(const FrameEvent & evt)
{
	/*if (mKeyboard->isKeyDown(OIS::KeyCode::KC_I) == false
		&& mKeyboard->isKeyDown(OIS::KeyCode::KC_J) == false
		&& mKeyboard->isKeyDown(OIS::KeyCode::KC_K) == false
		&& mKeyboard->isKeyDown(OIS::KeyCode::KC_L) == false)
	{
		velocity = 0;
	}

	Vector3 movement = Vector3::ZERO;
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_I))
	{
		velocity += 10 * evt.timeSinceLastFrame;
		movement.z -= 1 + velocity;
	}
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_K))
	{
		velocity += 10 * evt.timeSinceLastFrame;
		movement.z += 1 + velocity;
	}
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_J))
	{
		velocity += 10 * evt.timeSinceLastFrame;
		movement.x -= 1 + velocity;
	}
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_L))
	{
		velocity += 10 * evt.timeSinceLastFrame;
		movement.x += 1 + velocity;
	}
	cubeNode->translate(movement * evt.timeSinceLastFrame);

	Degree rotationDegree = Degree(60 * evt.timeSinceLastFrame);
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD4))
	{
		cubeNode->rotate(Vector3(0, 1, 0), Radian(rotationDegree));
	}
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD6))
	{
		cubeNode->rotate(Vector3(0, 1, 0), Radian(-rotationDegree));
	}
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD8))
	{
		cubeNode->rotate(Vector3(1, 0, 0), Radian(rotationDegree));
	}
	if (mKeyboard->isKeyDown(OIS::KeyCode::KC_NUMPAD2))
	{
		cubeNode->rotate(Vector3(1, 0, 0), Radian(-rotationDegree));
	}*/

	Degree rotationDegree = Degree(60 * evt.timeSinceLastFrame);
	float oldX = cubeNode->getPosition().x;
	float oldZ = cubeNode->getPosition().z;
	float newX = (oldX * Math::Cos(Radian(rotationDegree))) + (oldZ * Math::Sin(Radian(rotationDegree)));
	float newZ = (oldX * -Math::Sin(Radian(rotationDegree))) + (oldZ * Math::Cos(Radian(rotationDegree)));

	cubeNode->setPosition(newX, 0, newZ);


	return true;
}


//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
