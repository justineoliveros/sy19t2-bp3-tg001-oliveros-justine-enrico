/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	ManualObject *triangle = mSceneMgr->createManualObject();
	triangle->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_LIST);

	// Front 
	triangle->position(10, 10, 10);
	triangle->position(-10, -10, 10);
	triangle->position(10, -10, 10);

	triangle->position(10, 10, 10);
	triangle->position(-10, 10, 10);
	triangle->position(-10, -10, 10);

	// Back
	triangle->position(10, 10, -10);
	triangle->position(10, -10, -10);
	triangle->position(-10, -10, -10);

	triangle->position(10, 10, -10);
	triangle->position(-10, -10, -10);
	triangle->position(-10, 10, -10);

	// Right
	triangle->position(10, -10, -10);
	triangle->position(10, 10, -10);
	triangle->position(10, 10, 10);

	triangle->position(10, -10, 10);
	triangle->position(10, -10, -10);
	triangle->position(10, 10, 10);

	// Left
	triangle->position(-10, 10, 10);
	triangle->position(-10, -10, -10);
	triangle->position(-10, -10, 10);

	triangle->position(-10, 10, 10);
	triangle->position(-10, 10, -10);
	triangle->position(-10, -10, -10);
	
	// Top
	triangle->position(10, 10, -10);
	triangle->position(-10, 10, 10);
	triangle->position(10, 10, 10);

	triangle->position(-10, 10, -10);
	triangle->position(-10, 10, 10);
	triangle->position(10, 10, -10);

	// Bottom
	triangle->position(10, -10, -10);
	triangle->position(10, -10, 10);
	triangle->position(-10, -10, 10);

	triangle->position(-10, -10, -10);
	triangle->position(10, -10, -10);
	triangle->position(-10, -10, 10);


	triangle->end();
	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(triangle);
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
