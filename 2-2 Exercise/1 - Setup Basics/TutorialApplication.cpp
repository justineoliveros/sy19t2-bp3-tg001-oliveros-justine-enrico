/*
-----------------------------------------------------------------------------
Filename:    TutorialApplication.cpp
-----------------------------------------------------------------------------

This source file is part of the
   ___                 __    __ _ _    _
  /___\__ _ _ __ ___  / / /\ \ (_) | _(_)
 //  // _` | '__/ _ \ \ \/  \/ / | |/ / |
/ \_// (_| | | |  __/  \  /\  /| |   <| |
\___/ \__, |_|  \___|   \/  \/ |_|_|\_\_|
      |___/
Tutorial Framework (for Ogre 1.9)
http://www.ogre3d.org/wiki/
-----------------------------------------------------------------------------
*/

#include "TutorialApplication.h"
//---------------------------------------------------------------------------
TutorialApplication::TutorialApplication(void)
{
}
//---------------------------------------------------------------------------
TutorialApplication::~TutorialApplication(void)
{
}

ManualObject * TutorialApplication :: createCube(float size)
{
	ManualObject *triangle = mSceneMgr->createManualObject();
	triangle->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_LIST);

	// Front
	triangle->position(size, size, size); // UR
	triangle->position(size, -size, size); // LR
	triangle->position(-size, -size, size); // LL
	triangle->position(-size, size, size); // UL

	// Back
	triangle->position(size, size, -size); // UR
	triangle->position(size, -size, -size); // LR
	triangle->position(-size, -size, -size); // LL
	triangle->position(-size, size, -size); // UL

	// Front Index
	triangle->index(0);
	triangle->index(2);
	triangle->index(1);

	triangle->index(2);
	triangle->index(0);
	triangle->index(3);

	// Back Index
	triangle->index(4);
	triangle->index(5);
	triangle->index(6);

	triangle->index(6);
	triangle->index(7);
	triangle->index(4);

	// Right Index
	triangle->index(1);
	triangle->index(5);
	triangle->index(4);

	triangle->index(4);
	triangle->index(0);
	triangle->index(1);

	// Left Index
	triangle->index(3);
	triangle->index(6);
	triangle->index(2);

	triangle->index(7);
	triangle->index(6);
	triangle->index(3);

	// Top Index
	triangle->index(0);
	triangle->index(4);
	triangle->index(3);

	triangle->index(7);
	triangle->index(3);
	triangle->index(4);

	// Bottom Index
	triangle->index(1);
	triangle->index(2);
	triangle->index(5);

	triangle->index(6);
	triangle->index(5);
	triangle->index(2);

	// End drawing
	triangle->end();

	return triangle;
}

//---------------------------------------------------------------------------
void TutorialApplication::createScene(void)
{
    // Create your scene here :)
	ManualObject* manual = createCube(10.0f);

	mSceneMgr->getRootSceneNode()->createChildSceneNode()->attachObject(manual);
}
//---------------------------------------------------------------------------

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
    INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
    int main(int argc, char *argv[])
#endif
    {
        // Create application object
        TutorialApplication app;

        try {
            app.go();
        } catch(Ogre::Exception& e)  {
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            MessageBox(NULL, e.getFullDescription().c_str(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
            std::cerr << "An exception has occurred: " <<
                e.getFullDescription().c_str() << std::endl;
#endif
        }

        return 0;
    }

#ifdef __cplusplus
}
#endif

//---------------------------------------------------------------------------
