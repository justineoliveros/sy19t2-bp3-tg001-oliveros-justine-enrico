#pragma once
#include <OgreManualObject.h>
#include <OgreSceneNode.h>
#include <OgreSceneManager.h>

using namespace Ogre;

class Planet
{
public:
	Planet(SceneNode* node);
	Planet* createPlanet(SceneManager* sceneManager, float size, ColourValue colour);
	~Planet();

	void update(const FrameEvent& evt);

	SceneNode* getNode();
	void setParent(Planet* parent);

	Planet* getParent();

	void setLocalRotationSpeed(float speed);
	void setRevolutionSpeed(float speed);

private:
	SceneNode* mNode;
	Planet* mParent;
	float mLocalRotationSpeed;
	float mRevolutionSpeed;
};