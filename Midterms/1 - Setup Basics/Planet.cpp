#include "Planet.h"

Planet::Planet(SceneNode* node)
{
	mNode = node;
}

Planet* Planet::createPlanet(SceneManager* sceneManager, float size, ColourValue colour)
{
	Planet* newPlanet = new Planet(sceneManager->getRootSceneNode()->createChildSceneNode());
	//SceneNode* mNode = sceneManager->getRootSceneNode()->createChildSceneNode();
	ManualObject* myPlanet = sceneManager->createManualObject();
	myPlanet->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
	myPlanet->colour(colour);

	// Front
	myPlanet->position(size / 2, size / 2, size / 2); // UR
	myPlanet->position(size / 2, -size / 2, size / 2); // LR
	myPlanet->position(-size / 2, -size / 2, size / 2); // LL
	myPlanet->position(-size / 2, size / 2, size / 2); // UL

	// Back
	myPlanet->position(size / 2, size / 2, -size / 2); // UR
	myPlanet->position(size / 2, -size / 2, -size / 2); // LR
	myPlanet->position(-size / 2, -size / 2, -size / 2); // LL
	myPlanet->position(-size / 2, size / 2, -size / 2); // UL

	// Front Index
	myPlanet->index(0);
	myPlanet->index(2);
	myPlanet->index(1);

	myPlanet->index(2);
	myPlanet->index(0);
	myPlanet->index(3);

	// Back Index
	myPlanet->index(4);
	myPlanet->index(5);
	myPlanet->index(6);

	myPlanet->index(6);
	myPlanet->index(7);
	myPlanet->index(4);

	// Right Index
	myPlanet->index(1);
	myPlanet->index(5);
	myPlanet->index(4);

	myPlanet->index(4);
	myPlanet->index(0);
	myPlanet->index(1);

	// Left Index
	myPlanet->index(3);
	myPlanet->index(6);
	myPlanet->index(2);

	myPlanet->index(7);
	myPlanet->index(6);
	myPlanet->index(3);

	// Top Index
	myPlanet->index(0);
	myPlanet->index(4);
	myPlanet->index(3);

	myPlanet->index(7);
	myPlanet->index(3);
	myPlanet->index(4);

	// Bottom Index
	myPlanet->index(1);
	myPlanet->index(2);
	myPlanet->index(5);

	myPlanet->index(6);
	myPlanet->index(5);
	myPlanet->index(2);

	myPlanet->end();

	mNode = sceneManager->getRootSceneNode()->createChildSceneNode();
	mNode->attachObject(myPlanet);
	return new Planet(mNode);

}

Planet::~Planet()
{
}

void Planet::update(const FrameEvent& evt)
{
	mNode->rotate(Vector3(0, 1, 0), Radian(Degree(45 * mLocalRotationSpeed * evt.timeSinceLastFrame)));

	if (this->mParent != NULL)
	{
		Degree planetRevolution = Degree(15 * evt.timeSinceLastFrame * this->mRevolutionSpeed);
		Vector3 planetLocation = Vector3::ZERO;

		planetLocation.x = mNode->getPosition().x - mParent->mNode->getPosition().x;
		planetLocation.z = mNode->getPosition().z - mParent->mNode->getPosition().z;

		float OldX = planetLocation.x;
		float OldZ = planetLocation.z;

		float NewX = (OldX * Math::Cos(planetRevolution)) + (OldZ * Math::Sin(planetRevolution));
		float NewZ = (OldX * -Math::Sin(planetRevolution)) + (OldZ * Math::Cos(planetRevolution));

		this->getNode()->setPosition(getParent()->getNode()->getPosition().x + NewX,
			mNode->getPosition().y, getParent()->getNode()->getPosition().z + NewZ);
	}
}

SceneNode* Planet::getNode()
{
	return mNode;
}

void Planet::setParent(Planet* parent)
{
	mParent = parent;
}

Planet* Planet::getParent()
{
	return mParent;
}

void Planet::setLocalRotationSpeed(float speed)
{
	mLocalRotationSpeed = speed;
}

void Planet::setRevolutionSpeed(float speed)
{
	mRevolutionSpeed = speed;
}
